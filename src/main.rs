use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;
use std::thread;
use std::time::Duration;

use failure::Fail;
use serenity::{client::bridge::voice::ClientVoiceManager, model::prelude::*, prelude::*, voice};

#[macro_use]
extern crate log;

#[derive(serde::Deserialize, Debug, Clone)]
struct Config {
    status: Option<String>,
    notify_channel: Option<u64>,
    discord_token: String,
    audio_dir: PathBuf,
    join_audio_dir: Option<PathBuf>,
    join_audio_default_file: Option<PathBuf>,
}

#[derive(Debug, Fail)]
enum HandlerError {
    #[fail(display = "no audio with name")]
    AudioExistance,
    #[fail(display = "user not in voice")]
    UserAbsent,
    #[fail(display = "failed joining voice channel")]
    VoiceJoin,
    #[fail(display = "no guild id provided in gateway message")]
    GuildId,
    #[fail(display = "error reading audio: {:?}", _0)]
    Read(#[fail(cause)] std::io::Error),
}

impl Handler {
    fn handle_voice_join(
        &self,
        ctx: Context,
        guild: Option<GuildId>,
        user: UserId,
        channel: ChannelId,
    ) -> Result<(), HandlerError> {
        if let Some(notify) = self.config.notify_channel {
            let notify_chan: ChannelId = notify.into();
            notify_chan
                .send_message(&ctx.http, |m| {
                    m.content("Someone has joined somewhere");
                    m
                })
                .unwrap();
        }

        if let Some(dir) = &self.config.join_audio_dir {
            let mut path = dir.join("temporary_value");
            path.set_file_name(user.to_string());

            let path = if path.is_file() {
                path
            } else {
                self.config
                    .join_audio_default_file
                    .as_ref()
                    .ok_or(HandlerError::AudioExistance)?
                    .clone()
            };

            self.play(guild.ok_or(HandlerError::GuildId)?, channel, path)
        } else {
            Ok(())
        }
    }

    fn handle_play_command(&self, msg: Message) -> Result<(), HandlerError> {
        let mut path = self.config.audio_dir.join("temporary_value");
        path.set_file_name(msg.content[1..].trim());
        path.set_extension("pcm");

        if !path.is_file() {
            return Err(HandlerError::AudioExistance);
        }

        let vs = self.voice_states.lock();

        self.play(
            msg.guild_id.ok_or(HandlerError::GuildId)?,
            *vs.get(&msg.author.id).ok_or(HandlerError::UserAbsent)?,
            path,
        )
    }

    fn play(
        &self,
        guild_id: GuildId,
        chan_id: ChannelId,
        path: impl AsRef<Path>,
    ) -> Result<(), HandlerError> {
        self.playbacks
            .lock()
            .entry(guild_id)
            .and_modify(|x| *x += 1)
            .or_insert(1);

        let source = voice::pcm(
            true,
            BufReader::new(File::open(path).map_err(HandlerError::Read)?),
        );

        let manager_mutex = self.voice_manager.lock().as_ref().unwrap().clone();
        let mut manager = manager_mutex.lock();

        let handler = manager
            .join(guild_id, chan_id)
            .ok_or(HandlerError::VoiceJoin)?;

        let audio_mutex = handler.play_returning(source);

        let manager_mutex2 = manager_mutex.clone();
        let playbacks_mutex2 = self.playbacks.clone();

        thread::spawn(move || {
            loop {
                thread::sleep(Duration::from_millis(20));
                let audio = audio_mutex.lock();
                if audio.finished {
                    // Make sure nobody else has started playing something since we did
                    let mut playbacks = playbacks_mutex2.lock();
                    let count = playbacks.get_mut(&guild_id).unwrap();
                    *count -= 1;

                    if *count == 0 {
                        let mut manager = manager_mutex2.lock();
                        manager.remove(guild_id);
                    }
                    return;
                }
                // Encourage other threads (i.e. the one doing playback) to take the lock instead of us
                lock_api::MutexGuard::unlock_fair(audio);
            }
        });

        Ok(())
    }
}

struct Handler {
    config: Config,
    // This double arcmutex is kinda ridiculous...
    // We need to initialise the inner Arc<Mutex<ClientVoiceManager>> by getting it from client, which we need to pass a value to when we create it...
    voice_manager: Arc<Mutex<Option<Arc<Mutex<ClientVoiceManager>>>>>,
    voice_states: Arc<Mutex<HashMap<UserId, ChannelId>>>,
    bot_id: Arc<Mutex<Option<UserId>>>,
    playbacks: Arc<Mutex<HashMap<GuildId, u32>>>,
}

impl EventHandler for Handler {
    // Set a handler for the `message` event - so that whenever a new message
    // is received - the closure (or function) passed will be called.
    //
    // Event handlers are dispatched through a threadpool, and so multiple
    // events can be dispatched simultaneously.
    fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "!ping" {
            // Sending a message can fail, due to a network error, an
            // authentication error, or lack of permissions to post in the
            // channel, so log to stdout when some error happens, with a
            // description of it.
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!") {
                error!("Error sending message: {:?}", why);
            }
        } else if msg.content.starts_with('!') {
            info!(
                "Handling message \"{}\" from \"{}\"",
                msg.content, msg.author
            );
            if let Err(e) = self.handle_play_command(msg) {
                error!("Error in message handling: {}", e);
            }
        }
    }

    fn voice_state_update(&self, ctx: Context, guild_id: Option<GuildId>, state: VoiceState) {
        if Some(state.user_id) == *self.bot_id.lock() {
            return;
        }

        info!("Handling channel change of {}", state.user_id,);

        match state.channel_id {
            Some(cid) => self.voice_states.lock().insert(state.user_id, cid),
            None => self.voice_states.lock().remove(&state.user_id),
        };

        if let Some(cid) = state.channel_id {
            if let Err(e) = self.handle_voice_join(ctx, guild_id, state.user_id, cid) {
                error!("Error in message handling: {}", e);
            }
        }
    }

    // Set a handler to be called on the `ready` event. This is called when a
    // shard is booted, and a READY payload is sent by Discord. This payload
    // contains data like the current user's guild Ids, current user data,
    // private channels, and more.
    //
    // In this case, just print what the current user's username is.
    fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);
        ctx.shard.set_presence(
            self.config.status.as_ref().map(|s| Activity::playing(s)),
            OnlineStatus::Online,
        );

        *self.bot_id.lock() = Some(ready.user.id);

        // TODO initially populate the voice state map
    }
}

fn main() {
    env_logger::init();

    let config = match envy::prefixed("PIPCT_").from_env::<Config>() {
        Ok(config) => config,
        Err(error) => panic!("{:#?}", error),
    };

    assert!(config.audio_dir.is_dir());

    let vm: Arc<Mutex<Option<Arc<Mutex<_>>>>> = Default::default();
    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::new(
        &config.discord_token.clone(),
        Handler {
            config,
            voice_manager: vm.clone(),
            voice_states: Default::default(),
            bot_id: Default::default(),
            playbacks: Default::default(),
        },
    )
    .expect("Err creating client");

    *vm.lock() = Some(client.voice_manager.clone());

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    if let Err(why) = client.start_autosharded() {
        panic!("Client error: {:?}", why);
    }
}
