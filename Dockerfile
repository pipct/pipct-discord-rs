FROM rust:1-buster AS builder

# Add our source code
ADD . ./

# Build our application
RUN OPUS_STATIC=1 cargo build --release

# Now, we need to build our _real_ Docker container, copying in the binary
FROM debian:stable-slim

WORKDIR /app

ENV PIPCT_AUDIO_DIR /app/audio
ENV PIPCT_JOIN_AUDIO_DIR /app/join_audio
COPY ./audio ./audio
COPY ./join_audio ./join_audio

COPY --from=builder \
    /target/release/pipct-serenity \
    ./pipct-serenity

ENV PIPCT_STATUS Rust!
ENV RUST_LOG info
ENV PIPCT_JOIN_AUDIO_DEFAULT_FILE /app/audio/airhorn.pcm

# To be set by the user: PIPCT_DISCORD_TOKEN, potentially RUST_BACKTRACE

CMD /app/pipct-serenity
