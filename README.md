> The Beast adopted **new raiment** and studied the ways of **Time** and **Space** 
> and **Light** and the **Flow** of energy through the Universe. From its studies, 
> the Beast fashioned new structures from **oxidised metal** and proclaimed their glories. 
> And the Beast’s followers rejoiced, finding renewed purpose in these **teachings**.
> &mdash; *from The Book of Mozilla, 11:14*


    PIPCT Discord Bot
    Copyright (C) 2019  Jamie McClymont and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
